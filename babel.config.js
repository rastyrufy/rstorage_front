module.exports = {
	"plugins": [
    [
      "import",
      {
        "libraryName": "element-ui",
        "style": false
      }
    ]
  ],
  presets: [
    '@vue/app'
  ]
}
