import Vue from 'vue'
import App from './App.vue'

import store from './shared/store'
import router from './shared/router'
import BackendAPI from './shared/backend_api'

import {
	Table,
	TableColumn,
	Link,
	MenuItemGroup,
	Submenu,
	Pagination,
	Dialog,
	Autocomplete,
	Dropdown,
	DropdownMenu,
	DropdownItem,
	Menu,
	MenuItem,
	Input,
	Switch,
	Select,
	Option,
	OptionGroup,
	Button,
	ButtonGroup,
	DatePicker,
	Tooltip,
	Form,
	FormItem,
	Tabs,
	TabPane,
	Alert,
	Slider,
	Icon,
	Row,
	Col,
	Upload,
	Badge,
	Card,
	Carousel,
	CarouselItem,
	ColorPicker,
	Container,
	Main,
	Image,
	MessageBox,
	Message,
	Transfer,
} from 'element-ui'

import "element-ui/lib/theme-chalk/table.css"
import "element-ui/lib/theme-chalk/table-column.css"
import "element-ui/lib/theme-chalk/link.css"
import "element-ui/lib/theme-chalk/menu-item-group.css"
import "element-ui/lib/theme-chalk/submenu.css"
import "element-ui/lib/theme-chalk/select-dropdown.css"
import "element-ui/lib/theme-chalk/slider.css"
import "element-ui/lib/theme-chalk/switch.css"
import "element-ui/lib/theme-chalk/tab-pane.css"
import "element-ui/lib/theme-chalk/tabs.css"
import "element-ui/lib/theme-chalk/tooltip.css"
import "element-ui/lib/theme-chalk/upload.css"
import "element-ui/lib/theme-chalk/alert.css"
import "element-ui/lib/theme-chalk/autocomplete.css"
import "element-ui/lib/theme-chalk/badge.css"
import "element-ui/lib/theme-chalk/button.css"
import "element-ui/lib/theme-chalk/button-group.css"
import "element-ui/lib/theme-chalk/card.css"
import "element-ui/lib/theme-chalk/carousel.css"
import "element-ui/lib/theme-chalk/carousel-item.css"
import "element-ui/lib/theme-chalk/col.css"
import "element-ui/lib/theme-chalk/color-picker.css"
import "element-ui/lib/theme-chalk/container.css"
import "element-ui/lib/theme-chalk/date-picker.css"
import "element-ui/lib/theme-chalk/dialog.css"
import "element-ui/lib/theme-chalk/dropdown.css"
import "element-ui/lib/theme-chalk/dropdown-item.css"
import "element-ui/lib/theme-chalk/dropdown-menu.css"
import "element-ui/lib/theme-chalk/form.css"
import "element-ui/lib/theme-chalk/form-item.css"
import "element-ui/lib/theme-chalk/icon.css"
import "element-ui/lib/theme-chalk/image.css"
import "element-ui/lib/theme-chalk/input.css"
import "element-ui/lib/theme-chalk/main.css"
import "element-ui/lib/theme-chalk/message.css"
import "element-ui/lib/theme-chalk/message-box.css"
import "element-ui/lib/theme-chalk/option.css"
import "element-ui/lib/theme-chalk/option-group.css"
import "element-ui/lib/theme-chalk/pagination.css"
import "element-ui/lib/theme-chalk/popper.css"
import "element-ui/lib/theme-chalk/row.css"
import "element-ui/lib/theme-chalk/select.css"
import "element-ui/lib/theme-chalk/transfer.css"

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

import './assets/style.css'

locale.use(lang)

const components = [
	Table,
	TableColumn,
	Link,
	MenuItemGroup,
	Submenu,
	Pagination,
	Dialog,
	Autocomplete,
	Dropdown,
	DropdownMenu,
	DropdownItem,
	Menu,
	MenuItem,
	Input,
	Switch,
	Select,
	Option,
	OptionGroup,
	Button,
	ButtonGroup,
	DatePicker,
	Tooltip,
	Form,
	FormItem,
	Tabs,
	TabPane,
	Alert,
	Slider,
	Icon,
	Row,
	Col,
	Upload,
	Badge,
	Card,
	Carousel,
	CarouselItem,
	ColorPicker,
	Container,
	Main,
	Image,
	MessageBox,
	Message,
	Transfer
]

components.forEach(component => {
	Vue.component(component.name, component);
});

Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$message = Message;

Vue.prototype.$extensions = ["jpg", "jpeg", "png", "pdf", "rtxt", "mp4"];
Vue.prototype.$extensions_image = ["jpg", "jpeg", "png"]

Vue.use(BackendAPI, { base_url: process.env.VUE_APP_API_URL })

import Capitalize from './mixins/capitalize.js'
import Errors from './mixins/errors.js'

Vue.mixin(Capitalize)
Vue.mixin(Errors)

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app')
