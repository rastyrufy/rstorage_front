import { updateMark, toggleMark } from 'tiptap-commands'
import { Mark } from 'tiptap'

export default class Alignment extends Mark {

	get name() {
		return 'alignment'
	}

	get defaultOptions() {
		return {
			textAlign: ['left', 'center', 'right', 'justify'],
		}
	}

	get schema() {
		return {
			attrs    : {
				textAlign: {
					default: 'left',
				},
			},
			content  : 'inline*',
			defining : true,
			draggable: false,
			parseDOM : this.options.textAlign.map(align => ({
				tag  : 'span[style="text-align:'+align+'; display:block;"]',
				attrs: { textAlign: align },
			})),
			toDOM: node => {
					return ['span', { style : `text-align: ${node.attrs.textAlign}; display:block;` }, 0]
				}
		}
	}

	commands({ type }) {
		return (attrs) => {
			if (attrs.current_alignment && attrs.current_alignment.textAlign != attrs.textAlign)
				return updateMark(type, attrs)
			return toggleMark(type, {textAlign: attrs.textAlign})
		}
	}
}