export default {
	methods: {
		capitalize(str) {
			return str.toLowerCase().replace(/([a-z]+)|(_)/g, function(val, alpha, underscore, offset, the_string) {
				return (val == '_') ? ' ' :
						(offset == 1 || the_string[offset - 2] == ' ') ? val :
							val.charAt(0)
							.toUpperCase() + val
							.slice(1, val.length)
			})
		}
	}
}
