export default {
	methods: {
		auth_status(from) {
			if (typeof from === 'undefined')
				from = 'home'
			return this.$store.dispatch('inspect')
				.then(state => {
					if (state === false) {
						this.$router.replace({ name: 'login', params: { to: from } })
						return false
					}
					else {
						return true
					}
				})
		}
	}
}