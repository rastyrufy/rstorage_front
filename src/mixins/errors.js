export default {
	methods: {
		errors(err) {
			let errors = ''
			if (typeof err['response'] !== 'undefined') {
				if (err.response.data.length > 512) // Skip django page errors
					errors += 'BACKEND ERROR - Either route doesn\'t exist, or there is an issue in the backend.'
				else {
					Object.entries(err.response.data).forEach(entry => {
						let key = entry[0], error = entry[1]
						if (typeof error == 'string')
							errors += key + " - " + error + '<br>'
						else
							for (let i in error)
								errors += key + " - " + error[i] + '<br>'
					})
				}
			}
			else {
				errors = err.message
			}
			this.$message({
				type: 'error',
				dangerouslyUseHTMLString: true,
				message: errors
			})
		}
	}
}