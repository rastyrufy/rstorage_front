import axios from 'axios';

axios.defaults.withCredentials = true;

function getCookie(name) {
	var cookieValue = null;
	if (document.cookie && document.cookie !== '') {
		var cookies = document.cookie.split(';');
		for (var i = 0; i < cookies.length; i++) {
			var cookie = cookies[i].trim();
			// Does this cookie string begin with the name we want?
			if (cookie.substring(0, name.length + 1) === (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}

axios.interceptors.request.use(function (config) {
	var auth_token = localStorage.getItem('token');

	if (auth_token) {
		config.headers['Authorization'] = `RSTORAGE_AUTH ${auth_token}`;
	}
	config['WithCredentials'] = true;
	// console.log(config);
	var csrftoken = getCookie('csrftoken');
	if (csrftoken && csrftoken !== '') {
		config.headers['X-CSRFToken'] = csrftoken;
	}
	return config;
}, function (error) {
	return Promise.reject(error);
});

class BackendAPI {

	base_url = '';
	constructor(options) {
		this.base_url = options.base_url;
	}

	get(url, options) {
		return axios.get(`${this.base_url}${url}`, options);
	}

	post(url, data, post_detail) {
		return axios.post(`${this.base_url}${url}`, data, post_detail);
	}

	create(url, data) {
		return axios.create(`${this.base_url}${url}`, data);
	}

	patch(url, data) {
		return axios.patch(`${this.base_url}${url}`, data);
	}

	delete(url) {
		return axios.delete(`${this.base_url}${url}`);
	}
}

BackendAPI.install = function(Vue, options) {
	var instance  = new BackendAPI(options);
	Vue.prototype.$backend_api = instance;
}

export default BackendAPI;
