import Vue from 'vue'
import Vuex from 'vuex'
import JWTDecode from 'jwt-decode'

import BackendAPI from './backend_api'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		token: localStorage.getItem('token'),
		logged_in: false,
		endpoints: {
			obtain: 'login/',
			refresh: 'login/refresh/',
			verify: 'login/verify/'
		},

		reduce_item_count: false,
	},

	mutations: {
		update(state, credentials){
			localStorage.setItem('token', credentials.token);
			state.token = credentials.token;
			state.logged_in = true;
		},
		remove(state){
			localStorage.removeItem('token');
			state.token = null;
			state.logged_in = false;
		},

		do_reduce(state) {
			state.reduce_item_count = true;
		},

		done_reduce(state) {
			state.reduce_item_count = false;
		},
	},

	getters: {
		get_item_count_reducer(state) {
			return state.reduce_item_count
		},

		get_decoded_token(state) {
			if (state.token && state.token.split('.').length === 3) {
				return JWTDecode(state.token)
			}
			return false
		}
	},

	actions: {

		obtain(context, { username, password}){
			var credentials = {
				username: username,
				password: password
			}

			return this._vm.$backend_api.post(this.state.endpoints.obtain, credentials)
				.then(res => {
					var result = res.data;

					if (result.token) {
						this.commit('update', {
							token: result.token
						});

						return res;
					}
				})
				.catch(err => {
					return err;
				});
		},

		refresh(context) {
			var token = { token: this.state.token };

			return this._vm.$backend_api.post(this.state.endpoints.refresh, token)
				.then(res => {
					var result = res.data;

					if (result.token) {
						this.commit('update', {
							token: result.token
						});

						return true;
					}
				})
				.catch(err => {
					return false;
				});
		},

		verify(context) {
			var token = { token: this.state.token };

			return this._vm.$backend_api.post(this.state.endpoints.verify, token)
				.then(res => {
					if (res.token) {
						return true;
					}
				})
				.catch(err => {
					return false;
				});
		},

		inspect({getters}) {
			var decoded = getters.get_decoded_token
			if (decoded) {
				var expiry = decoded.exp;
				var created = decoded.orig_iat;

				let expire_time = expiry - (Date.now() / 1000)

				// if it is expiring in 30 min or less
				if (expire_time > 0 && expire_time < 1800) {
					return this.dispatch('refresh')
				}
				else if (expire_time >= 1800) {
					return true
				}
				// expired
				else {
					return false
				}
			}

			return false
		},

		logout(context) {
			this.commit('remove');
			return true;
		}
	}
})