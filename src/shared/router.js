import Vue from 'vue'
import Router from 'vue-router'

import Home from '../views/home'
import Login from '../views/login'
import Register from '../views/register'
import SocketTesting from '../views/SocketTesting'


Vue.use(Router)

let routes = [
	{ path: '/', name: 'home', component: Home },
	{ path: '/login', name: 'login', component: Login, props: true },
	{ path: '/register', name: 'register', component: Register, props: true },
	{ path: '/socket-test', name: 'socket-test', component: SocketTesting, props: true },
]

export default new Router({
	routes
})